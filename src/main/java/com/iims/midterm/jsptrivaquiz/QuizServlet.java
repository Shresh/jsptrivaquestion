package com.iims.midterm.jsptrivaquiz;

import com.iims.midterm.jsptrivaquiz.entity.Quiz;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static com.iims.midterm.jsptrivaquiz.connection.ConnectionFactory.getConnection;

@WebServlet(name = "quizServlet", value = "/trivaQuiz")
public class QuizServlet extends HttpServlet {

    public void init(){}

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        Quiz quiz = new Quiz();
        String id = request.getParameter("id");

        if(id == null){
            id = "1";
        }else{
            switch(id){
                case "1": id ="2";
                    break;
                case "2": id = "3";
                    break;
                case "3": id = "4";
                    break;
                case "4": id = "5";
                    break;
                case "5": id = "6";
                    break;
                case "6": id = "7";
                    break;
                case "7": id = "8";
                    break;
                case "8": id = "9";
                    break;
                case "9": id = "10";
                    break;
                case "10":
                    RequestDispatcher dispatcher = request.getRequestDispatcher("ThankYou.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        }



        try{
            System.out.println(id);
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT * FROM questions WHERE id = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                System.out.println(resultSet.getString("prasna"));

                quiz.setId(resultSet.getString("id"));
                quiz.setQuestion(resultSet.getString("prasna"));
                quiz.setOptionOne(resultSet.getString("option_one"));
                quiz.setOptionTwo(resultSet.getString("option_two"));
                quiz.setOptionThree(resultSet.getString("option_three"));
                quiz.setOptionFour(resultSet.getString("option_four"));
                quiz.setCorrectAnswer(resultSet.getString("correct_answer"));
            }else{
                request.setAttribute("error", "Something went wrong!!");
            }

            request.setAttribute("id",quiz.getId());
            request.setAttribute("question", quiz.getQuestion());
            request.setAttribute("optionOne", quiz.getOptionOne());
            request.setAttribute("optionTwo", quiz.getOptionTwo());
            request.setAttribute("optionThree", quiz.getOptionThree());
            request.setAttribute("optionFour", quiz.getOptionFour());
            request.setAttribute("correctAnswer", quiz.getCorrectAnswer());

        }catch(Exception e){
            System.out.println(e);
            request.setAttribute("error", e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("Quiz.jsp");
        dispatcher.forward(request, response);
    }
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Online Triva Quiz</title>
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <style>
        .option-button{
            margin-right: 350px !important;
            margin-left: 250px !important;
        }
    </style>
    <script>
        function showAnswer(chosenAnswer , correctAnswer){
            document.getElementById("next-button").removeAttribute('hidden');
            if(chosenAnswer == correctAnswer){
                let message = "Your selected answer is correct!!";
                window.alert(message)
            }else{
                let message = "Sorry you are wrong the answer is : " + correctAnswer;
                window.alert(message)
            }

        }
    </script>
</head>
<body id="page-top">
<header class="masthead">
    <div class="container px-4 px-lg-5 d-flex h-100 align-items-center justify-content-center">
        <div class="d-flex justify-content-center">

            <div class="text-center">
                <h1 style="font-size: 30px; ">${question}</h1>
                <table>
                    <tr>
                        <td class="option-button"><button style="margin-right: 250px !important; margin-left: 390px !important; margin-bottom: 20px !important;" class="btn btn-primary mx-auto mb-10 text-white-50" onclick="showAnswer('${optionOne}','${correctAnswer}')">${optionOne}</button></td>
                        <td class="option-button"><button style="margin-bottom: 20px !important;" class="btn btn-primary mx-auto mb-10 text-white-50" onclick="showAnswer('${optionTwo}','${correctAnswer}')">${optionTwo}</button></td>
                    </tr>
                    <tr>
                        <td class="option-button"><button style="margin-bottom: 20px !important; margin-right: 250px !important; margin-left: 390px !important;" class="btn btn-primary mx-auto mb-10 text-white-50" onclick="showAnswer('${optionThree}','${correctAnswer}')">${optionThree}</button></td>
                        <td class="option-button"><button style="margin-bottom: 20px !important;" class="btn btn-primary mx-auto mb-10 text-white-50" onclick="showAnswer('${optionFour}','${correctAnswer}')">${optionFour}</button></td>
                    </tr>
                </table>
                    <a class="btn btn-primary" id="next-button" href="http://localhost:8080/JSPTrivaQuiz_war_exploded/trivaQuiz?id=${id}" hidden> Next Question </a>
            </div>
        </div>
    </div>
</header>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>

</body>
</html>
